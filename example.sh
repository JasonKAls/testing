#!/bin/sh

# this set of rules is intended as best practices
# for shell scripting and other scripting languages


# general rules
# 1). lines shouldn't exceed 80 characters in width
# 2). camel case variables
# 3). under score separate functions
# 4). make sure to use spaces properly
# 5). global variables go at the top of the script
# 6). main loop should be functions and structures only
#     no variable declarations
# 7). no code should be found outside of a function
#     except the main loop
# 8). keep comments brief and only use when necessary
#     NO OVER COMMENTING!
# 9). there shouldn't be whitespace between lines with
#     the exception of after each function or if needed
#     to separate a block of variables from a block of code

someVar="jiggy"
someNumber="56"
arrayOfSpaceSeparatedVars="red blue yellow"
whileLoopPasses="7"

function_name_separated_by_underscores() 
{ 
	# Comments regarding what the function 
	# does, IF REALLY NECESSARY, can go here.
	# Notice that the opening curly brace {
	# goes on the line after the function
	# declaration. Also the function names
	# should be descriptive, and have 
	# underscore separators
	

	someInteger="56"
	someString="jiggy"
	thatString="jiggy widit"
	counter=0
	echo "starting test function"

	# string comparison
	if [ "$someVar" = "$someString"	] ; then
		echo "SOME VAR: $someVar, SOME STRING: $someString" 
	fi

	if [ "$someVar" != "$thatString" ] ; then
		echo "SOME VAR: $someVar, SOME STRING: $thatString" 
	fi

	# integer comparison -gt (greater than) -eq (equal to) -lt (less than)
	while [ "$someNumber" -eq "$someInteger" ] ; do
		echo ""
		counter=`expr $counter + 1`
		echo $counter
		if [ "$counter" -gt "$whileLoopPasses" ] ; then
			someInteger="0"	
		fi
	done	
	
	# case example
	case $someVar in 
		jiggy) echo that shit is JIGGY
		notjiggy) echo that shit is not JIGGY
		;;
	esac

}

# for loop example
for newVar in $arrayOfSpaceSeparatedVars ; do
	function_name_separated_by_underscores
done
